import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

@Component({
  selector: 'ms-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  started = false;
  moneyPerHour = '30';
  moneyMade = 0;
  timer: any;
  timeSpentMs = 0;

  constructor() {}

  ngOnInit() {}

  start() {
    this.started = true;
    this.timer = setInterval(() => {
      this.timeSpentMs++;
      this.moneyMade =
        this.timeSpentMs / (3600000 / parseInt(this.moneyPerHour));
    }, 1);
  }

  stop() {
    this.started = false;
    this.moneyMade = 0;
    this.timeSpentMs = 0;
    clearInterval(this.timer);
  }
}
