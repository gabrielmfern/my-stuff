import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ms-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  @Input() value = '';
  @Input() type = 'text';
  @Input() placeholder = '';
  @Input() autocomplete: string;
  @Input() disabled = false;

  @Output() valueChanges = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}

  onValueChanges(value: string) {
    this.valueChanges.emit(value);
  }
}
